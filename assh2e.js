import { ASSH2E } from "./module/config.js";
import ASSHItemSheet from "./module/sheets/ASSHItemSheet.js";

Hooks.once("init", function() {
    console.log("ASSH2E | Initialising AS&SH 2e System");
    CONFIG.ASSH2E = ASSH2E;
    console.log(CONFIG);
    Items.unregisterSheet("core", ItemSheet);
    Items.registerSheet("assh2e", ASSHItemSheet, {makeDefault: true});

});