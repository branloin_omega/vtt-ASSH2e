# Astonishing Swordsmen and Sorcerers of Hyperborea 2e (Unofficial) for Foundry VTT

An unofficial system for Foundry VTT that provides character sheets and virtual 
tabletop support for AS&SH 2e roleplaying game by North Wind Adventures.

This is only a fan project and is not associated with North Wind Adventures in
anyway.

You can purchase AS&SH 
[here](https://www.drivethrurpg.com/product/221806/Astonishing-Swordsmen--Sorcerers-of-Hyperborea-Compleat-Second-Edition)

## Reporting Issues, Errors, and Suggestions
Submit new issues [here](https://gitlab.com/foundry-modules/vtt-ASSH2e/-/issues)
or contact me through the official Foundry VTT Discord server in the
`#other-game-systems` channel (`branloin#6679`).
