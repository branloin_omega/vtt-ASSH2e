export const ASSH2E = {};

ASSH2E.weaponTypes = {
    melee: "Melee",
    missile: "Missile",
}

ASSH2E.weaponProperties = {

    ignoreShieldAC: "(↵) Ignore Shield AC",
    attackBonusPlate: "(Ω) Attack Bonus vs Plate",
    twoHanded: "(+) Two-Handed",
    defensive: "(↔) Defensive",
    damageVsCharge: "(^) x2 Damage Dice vs Charge",
    dismount: "(#) Chance to Dismount",
    mountedCharge: "(∇) x2 Dice vs Charging Opponents",
    heavyWarhorseCharge: "(o) Base Damage Increase While on Warhorse",
    strengthDamageAdjustment: "(⤢) Strength Damage Adjustment",
    strengthDamageAdjustmentDraw: "(⤤) Strength Damage Adjustment Given Draw"
};

ASSH2E.attackRates = ["1/2", "1/1", "3/2", "2/1", "5/2", "3/1"];