export default class ASSHItemSheet extends ItemSheet {
    
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            width:530,
            height:340,
            classes: ["assh2e", "sheet", "item"],
            resizable:true,
            tabs: [{navSelector: ".tabs", contentSelector: ".content", initial: "description"}]
        });
    }
    
    get template() {
        return `systems/assh2e/templates/sheets/${this.item.data.type}-sheet.html`;
    }

    getData(){
        const baseData = super.getData();
        
        let sheetData = {
            owner: this.item.isOwner,
            editable: this.isEditable,
            item: baseData.item,
            data: baseData.item.data.data,
            config: CONFIG.ASSH2E
        };

        console.log(sheetData);

        return sheetData;
    }
}